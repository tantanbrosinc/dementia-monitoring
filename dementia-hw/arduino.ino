#include <Wire.h>

#define ADDR_MAIN 1
#define ADDR_WIFI 6
#define ADDR_MOTOR 9

#define IN_PIR 12

#define ADDR_PUSHBUTTON 0
#define ADDR_SOUNDSENSOR 1
#define ADDR_PIRSENSOR 2

#define BUFF_LEN 3

int pirValue = 0;
String DataState = "";
char DataBuffer[BUFF_LEN];

void writeToWire(String name, String data, int address)
{
  Wire.beginTransmission(address);
  Wire.println(data);
  Wire.endTransmission();

  Serial.print("Write to wire => ");
  Serial.print(name);
  Serial.print(": ");
  Serial.println(data);
}

void onWireReceive(int bytes)
{
  String ssData = "";
  while (Wire.available())
  {
    char c = (char)Wire.read();
    ssData += c;
  }

  ssData.trim();
  Serial.print("Received Data From Main - Reading: ");
  Serial.println(ssData);
}

String addWireAddress(String data)
{
  return String(ADDR_WIFI) + String(",") + data;
}

void writeToWifi(String name, String data)
{
  writeToWire(name, data, ADDR_WIFI);
}

void printDivider()
{
  Serial.println();
  Serial.println("####################");
  Serial.println();
}

void setData(String name, String data)
{
	DataState.toCharArray(DataBuffer, BUFF_LEN);

	Serial.print("Setting Data => ");
  Serial.print(name);
  Serial.print(": ");
  Serial.println(data);
}

void sendPushButtonValue(int value){
	DataBuffer[0] = '0';
	DataBuffer[1] = ',';
	DataBuffer[2] = char(value) + '0';

	Serial.print("Setting Data => ");
	Serial.println(DataBuffer);

	// String data = String(ADDR_PUSHBUTTON) + String(',') + String(value);
	// setData("Push Button", data);
}

void sendSoundSensorValue(int value){
	DataBuffer[0] = '1';
	DataBuffer[1] = ',';
	DataBuffer[2] = char(value) + '0';

	Serial.print("Setting Data => ");
	Serial.println(DataBuffer);

	// String data = String(ADDR_SOUNDSENSOR) + String(',') + String(value);
	// setData("Sound Sensor", data);
}

void sendPIRSensorValue(int value) {
	DataBuffer[0] = '2';
	DataBuffer[1] = ',';
	DataBuffer[2] = char(value) + '0';

	Serial.print("Setting Data => ");
	Serial.println(DataBuffer);

	// String data = String(ADDR_PIRSENSOR) + String(',') + String(value);
	// setData("PIR Sensor", data);
}

void onWireRequest()
{
	Wire.write(DataBuffer);
	// DataState = "";
}

void setup()
{
  Serial.begin(9600);
  Serial.println("Main Setup Started...");

	Wire.begin(ADDR_MAIN);
  Wire.onReceive(onWireReceive);
  Serial.println("Set Wire - onWireReceive...");
  Wire.onRequest(onWireRequest);
  Serial.println("Set Wire - onWireRequest...");

	pinMode(IN_PIR, INPUT);

  Serial.println("Main Setup Finished...");
  printDivider();
}

void loop() 
{
	sendPushButtonValue(1);
	delay(500);
	sendPushButtonValue(0);
	delay(500);

	sendSoundSensorValue(1);
	delay(500);
	sendSoundSensorValue(0);
	delay(500);

	sendPIRSensorValue(1);
	delay(500);
	sendPIRSensorValue(0);
	delay(500);


	// pirValue = digitalRead(IN_PIR);

	// if(pirValue == HIGH) {
	// 	writeToWifi("PIR Sensor", "HIGH");
	// }else {
	// 	writeToWifi("PIR Sensor", "LOW");
	// }
}
