// #include <ESP8266WiFi.h>
#include <Wire.h>
#include "WifiHelper.h"
#include "WebSocketHelper.h"


#define ADDR_MAIN 1
#define ADDR_WIFI 6
#define ADDR_MOTOR 9


unsigned long curTime = 0;
unsigned long prevTime = 0;

void writeToWire(String name, String data, int address)
{
  Wire.beginTransmission(address);
  Wire.println(data);
  Wire.endTransmission();

  Serial.print("Write to wire => ");
  Serial.print(name);
  Serial.print(": ");
  Serial.println(data);
}

String addWireAddress(String data)
{
  return String(ADDR_WIFI) + String(",") + data;
}

void writeToMain(String name, String data)
{
  writeToWire(name, addWireAddress(data), ADDR_MAIN);
}

void printDivider()
{
  Serial.println();
  Serial.println("####################");
  Serial.println();
}

void onWireReceive(int bytes)
{
  String ssData = "";
  while (Wire.available())
  {
    char c = (char)Wire.read();
    ssData += c;
  }

  ssData.trim();
  Serial.print("Received Data From Main - Reading: ");
  Serial.println(ssData);

	Serial.print("Emit to Server: ");
	Serial.println(ssData);
	// emit(socketSendEvent, ssData);
}


void setup()
{
  Serial.begin(9600);
  Serial.println("Wifi Setup Started...");

	Wire.begin(D1, D2);

  // Wire.begin(ADDR_WIFI);
  // Wire.onReceive(onWireReceive);

  /* Wifi */
  connectToWifi();

  /* Web Socket */
  connectToWebSocket();

  Serial.println("Wifi Setup Finished...");
	printDivider();
}


void receiveFromWire()
{
	Wire.requestFrom(ADDR_MAIN, 13); /* request & read data of size 13 from slave */
	String ssData = "";
  while (Wire.available())
  {
    char c = (char)Wire.read();
    ssData += c;
  }

  ssData.trim();
  Serial.print("Received Data From Main - Reading: ");
  Serial.println(ssData);

	Serial.print("Emit to Server: ");
	Serial.println(ssData);
	delay(1000);
}

void onEveryWireRead()
{
	byte NUM_BYTES = 3;
  Wire.requestFrom(ADDR_MAIN, NUM_BYTES);
  String mainData = "";
  while (Wire.available())
  {
    char c = Wire.read();
    mainData += c;
  }
  Serial.println(mainData);
	emit(rawDataEvents[0], mainData);
}

void loop() 
{
	verifySocket();

	curTime = millis();
  if (curTime - prevTime > 100)
  {
    Serial.print("onEveryWireRead MainState ==> ");
    // 1 second passed
    onEveryWireRead();
    prevTime = curTime;
    // Serial.println("1 Second Passed");
  }
}
