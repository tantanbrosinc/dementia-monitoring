function calcMovingAverage(items, window) {
  if (items.length > 0) {
    let lastElems = items.slice(Math.max(items.length - window, 1));
    if (lastElems.length > 0) {
      let aveX = lastElems.reduce((sum, value) => sum + value.x, 0) / lastElems.length;
      let aveY = lastElems.reduce((sum, value) => sum + value.y, 0) / lastElems.length;
      return { x: Math.round(aveX), y: Math.round(aveY) };
    }
  }
  return { x: 0, y: 0 };
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

function queuePush(list, item, maxLength) {
  if (list && list.length > maxLength) {
    list.shift();
  }
  list.push(item);
}
