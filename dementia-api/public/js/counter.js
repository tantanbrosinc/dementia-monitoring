function Counter(id) {
  this.id = id;
  this.count = 0;
  this.debounceMS = 3000;
  this.canChange = true;
  this.maxTime = 3000;
  this.currentTime = 0;

  this.listeners = {
    onCountChanged: [],
    onZeroCount: [],
  };

  this.addEventListener = (id, callback) => {
    if (this.listeners[id]) {
      this.listeners[id].push(callback);
    }
  };

  this.onCountChanged = (count, source) => {
    let callbacks = this.listeners.onCountChanged;

    for (const callback of callbacks) {
      callback(this, { id, count, source });
    }
  };

  this.onZeroCount = (count, source) => {
    let callbacks = this.listeners.onZeroCount;

    for (const callback of callbacks) {
      callback(this, { id, count, source });
    }
  };

  this.trackCount = (newCount, source) => {
    if (this.canChange && this.count != newCount) {
      this.count = newCount;

      let timer = null;

      if (this.count == 0) {
        // start timer
        timer = setInterval(() => {
          this.currentTime += 1000;
          if (this.currentTime >= this.thresholdTime) {
            this.onZeroCount(this.count, source);
          }
        }, 1000);
      } else {
        // reset timer
        this.currentTime = 0;
        if (timer) {
          clearInterval(timer);
        }
      }

      this.onCountChanged(this.count, source);

      // debounce
      this.canChange = false;
      setTimeout(() => {
        this.canChange = true;
      }, this.debounceMS);
    }
  };
}
