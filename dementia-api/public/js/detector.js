function Detector(cameraName, cameraCanvas, fps) {
  // Sticky Face Detect
  const STICKY_WINDOW = 10;

  // Draw Face
  const AVERAGE_WINDOW = 2;
  // const AVERAGE_WINDOW = (fps * 1) / 3;

  this.cameraName = cameraName;
  this.cameraCanvas = cameraCanvas;
  this.prevStarts = [];
  this.prevEnds = [];
  this.midpoints = [];

  this.listeners = {
    onFaceDetected: [],
  };

  this.addEventListener = (id, callback) => {
    if (this.listeners[id]) {
      this.listeners[id].push(callback);
    }
  };

  this.onFaceDetected = faceCount => {
    let callbacks = this.listeners.onFaceDetected;
    // console.log(`onFaceDetected called with ${callbacks.length} callbacks`);

    for (const callback of callbacks) {
      callback(this, { cameraName: this.cameraName, faceCount });
    }
  };

  this.init = () => {
    this.camera = document.getElementById(this.cameraName);
    this.width = this.camera.width;
    this.height = this.camera.height;

    console.log(`Init: width=${this.width} height=${this.height}`);

    if (this.width <= 0 || this.height <= 0) {
      return;
    }
  };

  this.drawFace = (faceId, destination, face, ratio, useAverage = false, window = AVERAGE_WINDOW) => {
    // get history of points for specific face
    let usePrevStarts = this.prevStarts[faceId] || [];
    let usePrevEnds = this.prevEnds[faceId] || [];
    this.prevStarts[faceId] = usePrevStarts;
    this.prevEnds[faceId] = usePrevEnds;

    // Starting point of rectangle
    let xStart = face.x * ratio;
    let yStart = face.y * ratio;
    queuePush(usePrevStarts, { x: xStart, y: yStart }, window);

    // Ending point of rectangle
    let xEnd = xStart + face.width * ratio;
    let yEnd = yStart + face.height * ratio;
    queuePush(usePrevEnds, { x: xEnd, y: yEnd }, window);

    if (useAverage) {
      let aveStart = calcMovingAverage(usePrevStarts, window);
      if (aveStart !== null) {
        xStart = aveStart.x;
        yStart = aveStart.y;
      }
      let aveEnd = calcMovingAverage(usePrevEnds, window);
      if (aveEnd !== null) {
        xEnd = aveEnd.x;
        yEnd = aveEnd.y;
      }
    }

    let point1 = new cv.Point(xStart, yStart);
    let point2 = new cv.Point(xEnd, yEnd);

    let midpoint = { faceId, x: (xStart + xEnd) / 2, y: (yStart + yEnd) / 2, rectangle: { start: point1, end: point2 } };
    queuePush(this.midpoints, midpoint, STICKY_WINDOW);

    // cv.rectangle(destination, point1, point2, [255, 0, 255, 255]);
  };

  this.rawDraw = (faceId, destination, face, ratio) => {
    // Starting point of rectangle
    let xStart = face.x * ratio;
    let yStart = face.y * ratio;

    // Ending point of rectangle
    let xEnd = xStart + face.width * ratio;
    let yEnd = yStart + face.height * ratio;

    let point1 = new cv.Point(xStart, yStart);
    let point2 = new cv.Point(xEnd, yEnd);

    // cv.rectangle(destination, point1, point2, [0, 255, 255, 255]);
  };

  this.detectFaces = (destination, ratio) => {
    const faceCount = this.faces.size();
    this.onFaceDetected(faceCount);

    for (let i = 0; i < this.faces.size(); ++i) {
      let faceId = i;
      let face = this.faces.get(faceId);

      this.drawFace(faceId, destination, face, ratio);
      this.rawDraw(faceId, destination, face, ratio);
    }
  };

  this.run = classifier => {
    // don't compute if we don't have a source image
    if (this.width <= 0 || this.height <= 0) {
      return;
    }

    this.dst = new cv.Mat(this.height, this.width, cv.CV_8UC4);
    this.downscale = new cv.Mat(this.height, this.width, cv.CV_8UC4);
    this.gray = new cv.Mat();
    this.faces = new cv.RectVector();
    const ratio = 1.5;

    // console.time("timeit");

    // copy source to destination image
    let src = cv.imread(this.camera);
    src.copyTo(this.dst);

    this.downscaleImage(src, this.downscale, this.width, this.height, ratio);
    this.convertToGrayscale(this.downscale, this.gray);

    classifier.detectMultiScale(this.gray, this.faces, 1.2, 5, 0 | cv.CV_HAAR_SCALE_IMAGE);
    this.detectFaces(this.dst, ratio);

    this.drawSticky(this.dst);

    cv.imshow(this.cameraCanvas, this.dst);

    src.delete();
    this.dst.delete();
    this.downscale.delete();
    this.gray.delete();
    this.faces.delete();
    // console.timeEnd("timeit");
  };

  this.downscaleImage = (source, destination, width, height, ratio) => {
    let dsize = new cv.Size(width / ratio, height / ratio);
    cv.resize(source, destination, dsize, 0, 0, cv.INTER_AREA);
  };

  this.convertToGrayscale = (image, gray) => {
    cv.cvtColor(image, gray, cv.COLOR_RGBA2GRAY, 0);
  };

  this.drawSticky = destination => {
    let drawnRectangles = [];

    const pxRadius = 40; // pixels

    if (this.midpoints.length <= 0) return;

    let distinctWithinRadius = [];
    distinctWithinRadius.push(this.midpoints[0]);

    // get all midpoints that are not within a certain radius
    for (let i = 1; i < this.midpoints.length; i++) {
      let midpoint = this.midpoints[i];

      for (let j = 0; j < distinctWithinRadius.length; j++) {
        let distinctPoint = distinctWithinRadius[j];
        if (!isWithinRadius(distinctPoint, midpoint, pxRadius)) {
          queuePush(distinctWithinRadius, midpoint, 40);
        }
      }
    }

    // pag isa lang draw agad
    if (distinctWithinRadius.length == 1) {
      let midpoint = distinctWithinRadius[0];
      cv.rectangle(destination, midpoint.rectangle.start, midpoint.rectangle.end, [0, 255, 0, 255]);
    } else if (distinctWithinRadius.length > 1) {
      // draw non-overlapping rectangles
      for (let i = distinctWithinRadius.length - 1; i > 0; i--) {
        // if previous rectangle overlaps with the next, remove previous;
        let prevMP = distinctWithinRadius[i];
        let nextMP = distinctWithinRadius[i - 1];

        let d1 = getDiagonalLength(prevMP.rectangle) / 2;
        let d2 = getDiagonalLength(nextMP.rectangle) / 2;
        let greaterBound = d1 + d2;

        let midpointDistance = getDistance(prevMP, nextMP);

        const factor = 2;
        if (midpointDistance > greaterBound / factor && midpointDistance < greaterBound * 2) {
          let largerBox = prevMP;

          // foreach rectangles drawn, check if what we're about to draw is near any
          let foundNeighbor = false;
          for (let r = 0; r < drawnRectangles.length; r++) {
            if (getDistance(drawnRectangles[r], largerBox) < greaterBound / factor) {
              foundNeighbor = true;
              break;
            }
          }

          // draw only if not very near a neighbor rectangle
          // neighbor rectangles usually means duplicate boxes on the same face
          if (!foundNeighbor) {
            cv.rectangle(destination, largerBox.rectangle.start, largerBox.rectangle.end, [0, 255, 0, 255]);
            drawnRectangles.push(largerBox);
          }
        }
      }
    }
  };

  function getLargestRectangle(rectangles) {
    let largestRect = {
      rect: {},
      area: 0,
    };

    for (const rect of rectangles) {
      const area = getArea(rect);
      if (area >= largestRect.area) {
        largestRect = { rect, area };
      }
    }

    return largestRect.rect;
  }

  function getDistance(pointA, pointB) {
    let xSqrd = (pointB.x - pointA.x) * (pointB.x - pointA.x);
    let ySqrd = (pointB.y - pointA.y) * (pointB.y - pointA.y);
    let distance = Math.sqrt(xSqrd + ySqrd);
    return distance;
  }

  function isWithinRadius(pointA, pointB, radius) {
    let distance = getDistance(pointA, pointB);
    return distance <= radius;
  }

  function getDiagonalLength(rectangle) {
    let width = Math.abs(rectangle.end.x - rectangle.start.x);
    let height = Math.abs(rectangle.end.y - rectangle.start.y);
    let distance = Math.sqrt(width * width + height * height);
    return distance;
  }

  function getArea(rectangle) {
    let width = Math.abs(rectangle.end.x - rectangle.start.x);
    let height = Math.abs(rectangle.end.y - rectangle.start.y);
    let area = width * height;
    return area;
  }
}
