function onOpenCvReady() {
  console.log("OpenCV Ready");
  const fps = 30;
  cv["onRuntimeInitialized"] = () => {
    // load pre-trained classifiers
    let classifier = new cv.CascadeClassifier();
    let faceCascadeFile = "haarcascade_frontalface_default.xml";
    let utils = new Utils();
    utils.createFileFromUrl(faceCascadeFile, faceCascadeFile, () => {
      classifier.load(faceCascadeFile);
    });

    let camera1Container = document.getElementById("camera1container");
    let image1Elem = new Image();
    image1Elem.id = "camera1";
    image1Elem.hidden = true;
    image1Elem.src = "http://192.168.1.169:81/stream";
    image1Elem.crossOrigin = "anonymous";
    image1Elem.onload = function () {
      console.log("Image 1 loaded");
      let cam1Detector = createCameraTracker("camera1", "camera1Canvas", fps);

      setInterval(() => {
        cam1Detector.run(classifier);
      }, 1000 / fps);
    };
    camera1Container.appendChild(image1Elem);

    let camera2Container = document.getElementById("camera2container");
    let image2Elem = new Image();
    image2Elem.id = "camera2";
    image2Elem.src = "http://192.168.1.170:81/stream";
    image2Elem.hidden = true;
    image2Elem.crossOrigin = "anonymous";
    image2Elem.onload = function () {
      console.log("Image 2 loaded");
      let cam2Detector = createCameraTracker("camera2", "camera2Canvas", fps);

      setInterval(() => {
        cam2Detector.run(classifier);
      }, 1000 / fps);
    };
    camera2Container.appendChild(image2Elem);
  };

  handleDeviceEvents();
}

function createCameraTracker(name, canvas, fps) {
  let camCounter = new Counter(name);
  camCounter.addEventListener("onCountChanged", handleCameraCountChanged);
  camCounter.addEventListener("onZeroCount", handleCameraZeroCount);

  let camDetector = new Detector(name, canvas, fps);
  camDetector.addEventListener("onFaceDetected", (source, args) => {
    handleCameraFaceDetected(camCounter, source, args);
  });

  camDetector.init();

  return camDetector;
}

// const notifications = new EventNotification("notifications");

// source - Counter
// args - {id, count, source}
function handleCameraZeroCount(source, args) {
  const notifications = new EventNotification("notifications");

  notifications.addNotification(`Patient has been out of sight in ${args.source}`);
}

// source - Counter
// args - {id, count, source}
function handleCameraCountChanged(source, args) {
  const patientCount = args.count;
  const notifications = new EventNotification("notifications");

  if (patientCount > 0) {
    // let message = `[${args.source}] ${patientCount} patient${patientCount > 1 ? "s" : ""} detected`;
    let message = `Patient is being monitored in ${args.source}`;
    notifications.addNotification(message);
  }
}

// source - Detector
// args - {cameraName, faceCount}
function handleCameraFaceDetected(counter, source, args) {
  counter.trackCount(args.faceCount, args.cameraName);
}

function handleDeviceEvents() {
  const notifications = new EventNotification("notifications");

  var socket = io.connect("localhost:3000/socket/device");
  socket.on("notify", function (data) {
    console.log(data);

    const message = data.message;

    if (message) {
      notifications.addNotification(message);
    }
  });
}
