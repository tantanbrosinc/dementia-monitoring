function EventNotification(containerId) {
  this.containerId = containerId;
  this.container = document.getElementById(this.containerId);

  this.levelMap = {
    info: "alert alert-light",
  };

  this.init = () => {};

  this.addNotification = (message, level = "info") => {
    this.onNotificationReceived(level, message);
  };

  this.testNotification = () => {
    setInterval(() => {
      this.onNotificationReceived();
    }, 300);
  };

  this.onNotificationReceived = (level, message) => {
    const timestamp = Date.now();

    const now = new Date().toString("MMM d, yyyy hh:mm:ss tt");

    let notificationItem = document.createElement("div");
    notificationItem.className = this.levelMap[level];
    // notificationItem.innerText = `[${MM}/${dd}/${YYYY} ${hh}:${mm}:${ss}] ${message}`;
    notificationItem.innerText = `[${now}] ${message}`;
    this.container.appendChild(notificationItem);

    this.scrollToBottom();
  };

  this.scrollToBottom = () => {
    this.container.scrollTop = this.container.scrollHeight;
  };
}
