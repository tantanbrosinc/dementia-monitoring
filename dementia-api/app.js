var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

// // Spawn mock stream 1
// const streamFile1 = path.resolve("./mock/mock-stream1.js");
// console.log(streamFile1);

// const { spawn } = require("child_process");
// const stream1 = spawn("node", [streamFile1]);
// stream1.stdout.on("data", data => {
//   console.log(`[Stream 1]: ${data}`);
// });

// stream1.stderr.on("data", data => {
//   console.log(`[Stream 1]: ${data}`);
// });

// // Spawn mock stream 2
// const streamFile2 = path.resolve("./mock/mock-stream2.js");
// console.log(streamFile2);

// const stream2 = spawn("node", [streamFile2]);
// stream2.stdout.on("data", data => {
//   console.log(`[Stream 2]: ${data}`);
// });

// stream2.stderr.on("data", data => {
//   console.log(`[Stream 2]: ${data}`);
// });

// socket io
app.setSocket = function (io) {
  console.log("[Socket.IO] Set Socket");
  verifyDevice(io);
  receiveRawData(io);
  // sendNotification(io);
};

function receiveRawData(io) {
  io.on("connection", function (socket) {
    console.log(`SocketIO - Device Connected socketId: ${socket.id}`);

    socket.on("raw-data", function (data) {
      console.log({ data });
      emitNotification(io, data);
    });
  });
}

function emitNotification(io, data) {
  const messages = [
    "Push button pressed, patient is asking for help",
    "Loud Noise detected in patient's room",
    "Movement detected in patient's room",
  ];

  let split = data.split(",");
  let inputType = parseInt(split[0]);
  let inputValue = parseInt(split[1]);

  if (inputValue) {
    const message = messages[inputType];

    // Notify only when the state changes
    states[inputType].setState(inputValue, function (state) {
      console.log({ inputType, inputValue, message });
      io.of("/socket/device").emit("notify", { message });
    });
  }
}

const states = [
  new State(), // PushButton
  new State(), // Sound Sensor
  new State(), // PIR
];

function State() {
  this.state = null;
  this.canChange = true;
  this.setState = function (value, cb) {
    if (this.canChange) {
      this.state = value;
      this.canChange = false;
      setTimeout(() => {
        this.canChange = true;
      }, 3000);
      cb(this.state);
    }
  };
}

// function sendNotification(io) {
//   io.of("/socket/device").on("connection", function (socket) {
//     console.log(`SocketIO - Connected socketId: ${socket.id}`);

//     const messages = [
//       "Push button pressed, patient is asking for help",
//       "Loud Noise detected in patient's room",
//       "Movement detected in patient's room",
//     ];

//     setInterval(() => {
//       let data = `${Math.floor(Math.random() * 3)},${Math.floor(Math.random() * 2)}`;
//       let split = data.split(",");
//       let inputType = parseInt(split[0]);
//       let inputValue = parseInt(split[1]);

//       if (inputValue) {
//         const message = messages[inputType];
//         console.log({ inputType, inputValue, message });

//         socket.emit("notify", { message });
//       }
//     }, 1000);
//   });
// }

function verifyDevice(io) {
  var verifiedDevices = [];

  io.on("connection", function (socket) {
    // Devices must verify through this socket
    socket.on("verify", function (data) {
      console.log(`DeviceSocket - verify - socketId: ${socket.id}`);
      var deviceId = socket.id;
      var verified = verifiedDevices.includes(deviceId);

      if (data == "nodemcu" && !verified) {
        // Push to Verified Devices and send its Device ID indicating it was verified
        verifiedDevices.push(deviceId);
        io.to(`${deviceId}`).emit("verified", deviceId);
        console.log(`DeviceSocket - verify - Device at ${deviceId} was verified`);
      }
    });

    socket.on("disconnect", function () {
      // If socket disconnects and is in verifiedDevices, its a device. remove it
      if (verifiedDevices.includes(socket.id)) {
        verifiedDevices = verifiedDevices.filter(id => id != socket.id);
        console.log(`DeviceSocket - disconnect - Device at ${socket.id} was disconnected`);
      }
    });
  });
}

module.exports = app;
