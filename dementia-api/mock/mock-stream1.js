const stream = require("stream");
const http = require("http");
const Readable = stream.Readable;
const fs = require("fs");
const path = require("path");

// TODO: Replace Boundary
const boundary = "gc0p4Jq0M2Yt08jU534c0p";
const port = 4001;

//Must be implemented in the way of flow
//Otherwise, res will be closed in advance
//For the implementation of readable stream, please refer to: https://nodejs.org/api/stream.html ා stream ා implementing ﹣ a ﹣ readable ﹣ stream
class MockStream extends Readable {
  constructor(...arg) {
    super(...arg);
    this.count = 0;

    this.folder = "./assets/coco2";
    this.files = fs.readdirSync(path.resolve(this.folder)).sort();

    this.baseBuffer = Buffer.concat([
      new Buffer(`--${boundary}\r\n`),
      new Buffer("HTTP/1.1 200 OK"),
      new Buffer("Access-Control-Allow-Origin: *"),
      new Buffer("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept"),
      new Buffer("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS"),
      new Buffer("Content-Type: image/jpeg\r\n\r\n"),
    ]);
  }
  async _read() {
    // read file

    const fileName = path.resolve(`${this.folder}/${this.files[this.count++]}`);
    const image = fs.readFileSync(fileName).toString("base64");
    const frame = Buffer.from(image, "base64");

    // assign buffer
    const result = Buffer.concat([this.baseBuffer, frame]);

    if (this.count >= this.files.length) {
      this.count = 0;
    }

    // control fps dito
    const fps = 30;
    setTimeout(() => {
      this.push(result);
    }, 1000 / fps);
  }
}

const server = http.createServer((req, res) => {
  //First output response header
  res.writeHead(200, {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
    "Access-Control-Allow-Methods": "GET,POST,PUT,DELETE,OPTIONS",
    "Content-Type": `multipart/x-mixed-replace; boundary="${boundary}"`,
  });
  const stream = new MockStream();
  stream.pipe(res);
});

server.listen(port);
server.on("listening", onListening);

function onListening() {
  console.log(`Listening on port ${port}`);
}
